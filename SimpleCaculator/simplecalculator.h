#ifndef SIMPLECALCULATOR_H
#define SIMPLECALCULATOR_H

#include <QMainWindow>

namespace Ui {
class SimpleCalculator;
}

class SimpleCalculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit SimpleCalculator(QWidget *parent = nullptr);
    //void resetTriggers();
    ~SimpleCalculator();

private:
    Ui::SimpleCalculator *ui;
    /*double displayVal;
    bool* PlusTrigger;
    bool* MinusTrigger;
    bool* MulTrigger;
    bool* DivTrigger;*/

private slots:
    void NumPressed();
    void MathPressed();
    void EqualsPressed();
    void ClearPressed();
    void ChangeSignPressed();
    void DotPressed();
    void DelPressed();
};

#endif // SIMPLECALCULATOR_H
