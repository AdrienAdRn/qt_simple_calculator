#include "simplecalculator.h"
#include "ui_simplecalculator.h"
#include <QString>
//#include <cmath>

double displayVal = 0.0;
bool PlusTrigger = false;
bool MinusTrigger = false;
bool MulTrigger = false;
bool DivTrigger = false;
bool DefaultDisplay = true;


SimpleCalculator::SimpleCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SimpleCalculator)
{
    ui->setupUi(this);

    // connection signals and slots
    // NumPressed Slot
    ui->Display->setText(QString::number(0.0));
    QPushButton* buttons[10];
    for(int i= 0;i<10;i++){
        QString buttonName = "Button_" + QString::number(i);
        buttons[i] = SimpleCalculator::findChild<QPushButton*>(buttonName);
        QObject::connect(buttons[i], SIGNAL(released()), this,SLOT(NumPressed()));
    }

    // MathPressed Slot
     QObject::connect(ui->Minus, SIGNAL(released()), this,SLOT(MathPressed()));
     QObject::connect(ui->Plus, SIGNAL(released()), this,SLOT(MathPressed()));
     QObject::connect(ui->Multiply, SIGNAL(released()), this,SLOT(MathPressed()));
     QObject::connect(ui->Divide, SIGNAL(released()), this,SLOT(MathPressed()));

     // EqualsPressed slot
     QObject::connect(ui->Equals, SIGNAL(released()), this,SLOT(EqualsPressed()));

     //ClearPressed slot
     QObject::connect(ui->Clear_Button, SIGNAL(released()), this,SLOT(ClearPressed()));

     //DotPressed slot
     QObject::connect(ui->Button_Dot, SIGNAL(released()), this,SLOT(DotPressed()));

     // DelPressed slot
     QObject::connect(ui->Delete, SIGNAL(released()), this,SLOT(DelPressed()));

     //ChangeSignPressed slot
     QObject::connect(ui->ChangeSign, SIGNAL(released()), this,SLOT(ChangeSignPressed()));
}

SimpleCalculator::~SimpleCalculator()
{
    delete ui;
}


// slots implementation
void SimpleCalculator::NumPressed()
{


    // retrieve sender button and its data
    QPushButton* senderButton = (QPushButton*)sender();
    double buttonVal=senderButton->text().toDouble();
    QString buttonText = QString::number(buttonVal);

    QString displayText = ui->Display->text();

    // replace default value or concatenate value
    if(DefaultDisplay){
        ui->Display->setText(buttonText);

        // set default display flag to false
        DefaultDisplay = false;
    } else {
         ui->Display->setText(displayText + buttonText);
    }
}

void SimpleCalculator::MathPressed()
{
    // reset all triggers
    PlusTrigger = false;
    MinusTrigger = false;
    MulTrigger = false;
    DivTrigger = false;

    // get info from sender and update trigger
    QPushButton* senderButton = (QPushButton*)sender();
    QString buttonText = senderButton->text();
    if(buttonText == "*"){
        displayVal = ui->Display->text().toDouble(); // store current value
        ui->Display->setText("");
        MulTrigger = true;

    } else if(buttonText == "/"){
        displayVal = ui->Display->text().toDouble(); // store current value
        ui->Display->setText("");
        DivTrigger = true;

    } else if(buttonText == "+"){
        displayVal = ui->Display->text().toDouble(); // store current value
        ui->Display->setText("");
        PlusTrigger = true;

    } else if(buttonText == "-"){
         displayVal = ui->Display->text().toDouble(); // store current value
         ui->Display->setText("");
         MinusTrigger = true;
        }
}

void SimpleCalculator::EqualsPressed()
{
    double current_val = ui->Display->text().toDouble(); // TODO : case not a number
    double output;
    if(MinusTrigger){
        output = displayVal - current_val;
        ui->Display->setText(QString::number(output));

    } else if(PlusTrigger){
        output = displayVal + current_val;
        ui->Display->setText(QString::number(output));

    } else if(MulTrigger){
        output = displayVal * current_val;
        ui->Display->setText(QString::number(output));

    } else if(DivTrigger){
        double epsilon = 0.00000000000001;
        if(current_val>epsilon){
            output = displayVal - current_val;
            ui->Display->setText(QString::number(output));
        } else {
            ui->Display->setText("ERROR DIVISON BY ZERO");
        }
    }

    // reset all triggers
    PlusTrigger = false;
    MinusTrigger = false;
    MulTrigger = false;
    DivTrigger = false;
}

void SimpleCalculator::ClearPressed()
{
    displayVal = 0.0;
    ui->Display->setText(QString::number(displayVal));

    // reset all triggers
    PlusTrigger = false;
    MinusTrigger = false;
    MulTrigger = false;
    DivTrigger = false;
}

void SimpleCalculator::ChangeSignPressed()
{
    QString current_text = ui->Display->text();
    QRegExp reg("[-]?[0-9]*");
    // ui->Display->text() != ""
    if(reg.exactMatch(current_text)){
        double current_val = ui->Display->text().toDouble();
        current_val = current_val*(-1);
        ui->Display->setText(QString::number(current_val));
    }
}

void SimpleCalculator::DotPressed()
{
    QString currentText = ui->Display->text();
    if(currentText != ""){ui->Display->setText(currentText + ".");}
}

void SimpleCalculator::DelPressed()
{
    std::string currentText = ui->Display->text().toStdString();

    // case smallest text displayed, go back to default display
    if(currentText.size() <= 1){
        DefaultDisplay = true;
        double default_val = 0.0;
         ui->Display->setText(QString::number(default_val));
    }
    // otherwise shrink current diplayed value
    else {
        currentText = currentText.substr(0, currentText.size()-1);

        QString output_text = QString::fromStdString(currentText);
        ui->Display->setText(output_text);
    }

}
